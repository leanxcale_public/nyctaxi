
package com.leanxcale.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.leanxcale.exception.LeanxcaleException;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/default")
public interface GISDemoController {
		
	@GetMapping("/run")
    public void runDemo(@RequestParam(value = "frequency", required=false) String frequency, @RequestParam(value= "turbo", required=false) String turbo) throws Exception;
	
	@GetMapping("/stop")
	public void stopDemo();
	
	@GetMapping("/clean")
	public void cleanDemo() throws Exception;
	
	@GetMapping("/status")
	public String getStatus();
}
