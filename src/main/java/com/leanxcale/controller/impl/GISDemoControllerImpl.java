package com.leanxcale.controller.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.leanxcale.constants.GISDemoConstants;
import com.leanxcale.controller.GISDemoController;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.service.GISDemoFunctionalService;

@RestController
@RequestMapping("/NYCTaxi")
public class GISDemoControllerImpl implements GISDemoController {

	@Autowired
	private GISDemoFunctionalService gisDemoFunctionalService;
	
	private String lxUrl;
	private int insertionPerSec = 0;
	private boolean turbo = false;
	private Map<String, Map<String,String>> rootProperties = null;
	
	private static AtomicReference<String> status = new AtomicReference<String>(GISDemoConstants.STATUS_STOPPED);

	public String getLxUrl() {
		return lxUrl;
	}

	public void setLxUrl(String lxUrl) {
		this.lxUrl = lxUrl;
	}

	public int getInsertionPerSec() {
		return insertionPerSec;
	}

	public boolean isTurbo() {
		return turbo;
	}

	public void setInsertionPerSec(int insertionPerSec) {
		this.insertionPerSec = insertionPerSec;
	}

	public Map<String, Map<String, String>> getRootProperties() {
		return rootProperties;
	}

	public void setRootProperties(Map<String, Map<String, String>> rootProperties) {
		this.rootProperties = rootProperties;
	}

	@Override
	public void runDemo(String frequency, String turbo) throws Exception {
		if (status.compareAndSet(GISDemoConstants.STATUS_CLEAN, GISDemoConstants.STATUS_RUNNING) ||
				status.compareAndSet(GISDemoConstants.STATUS_STOPPED, GISDemoConstants.STATUS_RUNNING)) {
			if (getRootProperties() == null) setRootProperties(loadRootProperties("gisDemo.properties"));
			if (frequency != null && !frequency.isEmpty()) {
				insertionPerSec = Integer.parseInt(frequency);
			}
			if (insertionPerSec == 0) {
				if (turbo != null && !turbo.isEmpty()) {
					this.turbo = Boolean.parseBoolean(turbo);
				}
			}
			for (Map.Entry<String, Map<String, String>> entry : getRootProperties().entrySet()) {
				Thread t = new Thread(() -> {
					try {
						gisDemoFunctionalService.runDemo(
								getLxUrl(),
								entry.getKey(),
								entry.getValue().get("database"),
								entry.getValue().get("user"),
								entry.getValue().get("password"),
								entry.getValue().get("dataset"),
								status,
								getInsertionPerSec(),
								isTurbo());
					}
					catch (Exception e) {
						status.compareAndSet(GISDemoConstants.STATUS_RUNNING, GISDemoConstants.STATUS_STOPPED);
						e.printStackTrace();
					}
				});
				t.start();
			}
		}
	}

	@Override
	public void stopDemo() {
		if (status.compareAndSet(GISDemoConstants.STATUS_RUNNING,GISDemoConstants.STATUS_STOPPED)) {
			insertionPerSec = 0;
			turbo = false;
		}
	}

	@Override
	public void cleanDemo() throws Exception {
		try {
			if (status.compareAndSet(GISDemoConstants.STATUS_STOPPED,GISDemoConstants.STATUS_CLEAN)) {
				if (getRootProperties() == null) setRootProperties(loadRootProperties("gisDemo.properties"));
				for (Map.Entry<String, Map<String,String>> entry : getRootProperties().entrySet()) {
					String key = (String)entry.getKey();
					gisDemoFunctionalService.cleanDemo(getLxUrl(), key, rootProperties.get(key).get("database"),
							rootProperties.get(key).get("user"), rootProperties.get(key).get("password"));
				}
				insertionPerSec = 0;
				turbo = false;
			}
		}
		catch (Exception e) {
			status.compareAndSet(GISDemoConstants.STATUS_CLEAN,GISDemoConstants.STATUS_STOPPED);
			throw e;
		}
	}

	private Map<String, Map<String, String>> loadRootProperties(String name) throws IOException {
		Properties prop = new Properties();
		// Try to find external properties
		String currentDir = System.getProperty("user.dir");
		System.out.println("user.dir="+currentDir);
		InputStream in;
		try {
			in = new FileInputStream(currentDir + "/" + name);
		}
		catch (FileNotFoundException e) {
		// External properties not found. Get the internal one
			System.out.println("External file not found");
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
		}
		prop.load(in);
		Map<String, Map<String,String>> rootProperties = new HashMap<String, Map<String,String>>();
		Enumeration<String> enums = (Enumeration<String>) prop.propertyNames();
		while (enums.hasMoreElements()) {
			String key = enums.nextElement();
			String value = prop.getProperty(key);
			if (key.equals("lxUrl")) {
				setLxUrl(value);
			}
			else if (key.equals("insertionPerSec")) {
				setInsertionPerSec(Integer.parseInt(value));
			}
			else {
				String[] parts = key.split("\\.");
				if (rootProperties.get(parts[0]) == null) {
					rootProperties.put(parts[0], new HashMap<String, String>());
					rootProperties.get(parts[0]).put("database", (String)prop.get(parts[0]+".database"));
					rootProperties.get(parts[0]).put("dataset", (String)prop.get(parts[0]+".dataset"));
					rootProperties.get(parts[0]).put("user", (String)prop.get(parts[0]+".user"));
					rootProperties.get(parts[0]).put("password", (String)prop.get(parts[0]+".password"));
				}
			}
		}
		return rootProperties;
	}

	@Override
	public String getStatus() {
		return status.get();
	}

}
