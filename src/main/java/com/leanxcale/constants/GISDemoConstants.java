package com.leanxcale.constants;

public interface GISDemoConstants {
	
	public static final String STATUS_RUNNING = "running";
	public static final String STATUS_STOPPED = "stopped";
	public static final String STATUS_CLEAN = "clean";

	public static final String TABLE = "TRIP_DATA";
	public static final String TABLE_WEEKLY = "TRIP_DATA_WEEKLY_GIS";
	public static final String TABLE_DAILY = "TRIP_DATA_DAILY_GIS";
	public static final String TABLE_HOURLY = "TRIP_DATA_HOURLY_GIS";
}
