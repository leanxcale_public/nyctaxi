package com.leanxcale.service;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import com.leanxcale.exception.LeanxcaleException;

public interface GISDemoFunctionalService {
	
    public void runDemo(String lxUrl, String name, String database, String usr, String passwd, String dataset, AtomicReference status, int insertionPerSec, boolean turbo) throws Exception;
	
	public void cleanDemo(String lxUrl, String name, String database, String usr, String passwd) throws Exception;

}
