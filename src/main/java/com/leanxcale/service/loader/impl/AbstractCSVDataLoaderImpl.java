package com.leanxcale.service.loader.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import com.leanxcale.constants.GISDemoConstants;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Credentials;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import com.leanxcale.service.loader.DataLoader;

public abstract class AbstractCSVDataLoaderImpl implements DataLoader {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public int loadData(String lxUrl, String path, AtomicReference<String> status, String database, String user, String password, int offset, int insertionPerSec, boolean turbo) throws Exception {


            String line;
            String cvsSplitBy = ",";

            Settings settings = new Settings();
            Credentials credentials = new Credentials();
            credentials.setDatabase(database).setUser(user);
            credentials.setDatabase(database).setPass(password.toCharArray());
            settings.credentials(credentials);
            if (!turbo) { // If we don't want transactionality, check turbo parameter
                settings.transactional();
                settings.deferSessionFactoryClose(5000);
            }

            int lineNumber = 1;

            try (Session session = SessionFactory.newSession(lxUrl, settings);
                 BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)))) {

                if (hasHeader() && offset == 0) {
                    // Skip the header
                    br.readLine();
                    lineNumber++;
                }
                // If we want to start from the last stored row, skip the previous ones
                if (offset > 0) {
                    while (lineNumber < offset) {
                        br.readLine();
                        lineNumber++;
                    }
                }

                if (insertionPerSec == 0 || turbo) { // Inserting with no real time period, with or without transactionality
                    String nextLine;
                    while ((nextLine = br.readLine()) != null && status.get().equals(GISDemoConstants.STATUS_RUNNING)) {
                        processLine(nextLine.split(cvsSplitBy),session);
                        if (lineNumber % getCommitThreshold() == 0) {
                            session.commit();
                            System.out.println("Commit "+lineNumber);
                        }
                        lineNumber++;
                        System.out.println("Commit "+lineNumber);
                    }
                    session.commit();
                }
                else { // Insert in real time or real time accelerated
                    List<String[]> lines = new ArrayList();

                    line = br.readLine(); // Get first line

                    if (line != null) {
                        // Add first line to lines list
                        lines.add(line.split(cvsSplitBy));
                        // While the thread is running
                        while (status.get().equals(GISDemoConstants.STATUS_RUNNING)) {

                            // Get the first stored line in the lines list
                            String[] parts = lines.get(0);

                            // Get first row datetime
                            LocalDateTime dateTimePickupLocal = LocalDateTime.parse(parts[0], formatter);
                            ZonedDateTime dateTimePickup = dateTimePickupLocal.atZone(ZoneId.of("America/Los_Angeles"));
                            // Get first row datetime plus 1
                            ZonedDateTime dateTimePickupPlus1 = dateTimePickup.plusSeconds(insertionPerSec);
                            Timestamp end = Timestamp.from(dateTimePickupPlus1.toInstant());
                            String secondPlus1Line = null;

                            LocalDateTime batchEndTime = LocalDateTime.now().plusSeconds(1);

                            // Add to lines the lines with timestamp before first row datetime plus insertionPerSec parameter: this-group
                            String[] newLineParts = null;
                            boolean found = false;
                            while (!found && (secondPlus1Line = br.readLine()) != null) {
                                newLineParts = secondPlus1Line.split(cvsSplitBy);
                                LocalDateTime lineDatetimeLocal = LocalDateTime.parse(newLineParts[0], formatter);
                                ZonedDateTime lineDatetime = lineDatetimeLocal.atZone(ZoneId.of("America/Los_Angeles"));
                                Timestamp lineTimestamp = Timestamp.from(lineDatetime.toInstant());
                                if (lineTimestamp.before(end)) {
                                    lines.add(newLineParts); // If it belongs to this-group
                                } else {
                                    found = true; // If it's higher, add later to lines to start again
                                }
                            }
                            // Store all this-group lines
                            for (String[] newParts : lines) {
                                processLine(newParts, session);
                                lineNumber++;
                            }
                            session.commit();
                            System.out.println("Commit "+lineNumber);
                            // Clear lines list and add the first one that was higher than this-group
                            lines.clear();
                            lines.add(newLineParts);

                            // Wait for next second if necessary
                            while (!LocalDateTime.now().isAfter(batchEndTime)) {
                                Thread.sleep(10);
                            }
                        }
                    }
                }
            }
        if (status.get().equals(GISDemoConstants.STATUS_STOPPED)){
            return lineNumber;
        }
        else {
            return 0;
        }
    }

    public abstract void processLine(String[] rowParts, Session session) throws Exception;

    public abstract boolean hasHeader();

    public abstract int getCommitThreshold();

}
