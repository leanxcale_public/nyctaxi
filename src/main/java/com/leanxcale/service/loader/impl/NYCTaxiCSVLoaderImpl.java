package com.leanxcale.service.loader.impl;

import com.leanxcale.constants.GISDemoConstants;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.tuple.Tuple;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class NYCTaxiCSVLoaderImpl extends AbstractCSVDataLoaderImpl {

    private static final boolean HAS_HEADER = false;
    private static final int COMMIT_THRESHOLD = 10000;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private Table table;
    private Table tableWeekly;
    private Table tableDaily;
    private Table tableHourly;


    @Override
    public void processLine(String[] rowParts, Session session) throws Exception {

        if(table == null){
          initTables(session);
        }

        LocalDateTime dateTimePickupLocal = LocalDateTime.parse(rowParts[0],formatter);
        //ZonedDateTime dateTimePickup = dateTimePickupLocal.atZone(ZoneId.of("America/Los_Angeles"));
        //Timestamp pickup =  Timestamp.from(dateTimePickup.toInstant());
        Timestamp pickup = Timestamp.valueOf(dateTimePickupLocal);
        int weekPickup = dateTimePickupLocal.getDayOfYear()/7;
        int dayPickup = dateTimePickupLocal.getDayOfWeek().getValue();
        int hourPickup = dateTimePickupLocal.getHour();

        LocalDateTime dropoffDateTimeLocal = LocalDateTime.parse(rowParts[6],formatter);
        //ZonedDateTime dropoffDateTime = dropoffDateTimeLocal.atZone(ZoneId.of("America/Los_Angeles"));
        Timestamp dropoff = Timestamp.valueOf(dropoffDateTimeLocal);

        double score = ThreadLocalRandom.current().nextDouble(0,5);

        long medallion = 2000000 + Long.parseLong(rowParts[1])%1000;

        Tuple tuple = table.createTuple();
        tuple.putTimestamp("PICKUP_DATETIME", pickup);
        tuple.putLong("MEDALLION", medallion);
        tuple.putLong("HACK_LICENSE", Long.parseLong(rowParts[2]));
        tuple.putString("VENDOR_ID", rowParts[3]);
        tuple.putInteger("RATE_CODE", Integer.parseInt(rowParts[4]));
        tuple.putString("STORE_AND_FWD_FLAG", rowParts[5]);
        tuple.putTimestamp("DROPOFF_DATETIME",dropoff);
        tuple.putInteger("PASSENGER_COUNT",Integer.parseInt(rowParts[7]));
        tuple.putInteger("TRIP_TIME_IN_SECS", Integer.parseInt(rowParts[8]));
        tuple.putDouble("TRIP_DISTANCE", Double.parseDouble(rowParts[9]));
        tuple.putDouble("PICKUP_LONGITUDE", Double.parseDouble(rowParts[10]));
        tuple.putDouble("PICKUP_LATITUDE", Double.parseDouble(rowParts[11]));
        tuple.putDouble("DROPOFF_LONGITUDE", Double.parseDouble(rowParts[12]));
        tuple.putDouble("DROPOFF_LATITUDE", Double.parseDouble(rowParts[13]));
        tuple.putDouble("SCORE",score);

        try {
            table.insert(tuple);

            Tuple weekTuple = tableWeekly.createTuple();
            weekTuple.putInteger("WEEK", weekPickup);
            weekTuple.putLong("TRIPS", 1L);

            tableWeekly.upsert(weekTuple);

            Tuple dayTuple = tableDaily.createTuple();
            dayTuple.putInteger("DAY", dayPickup);
            dayTuple.putLong("TRIPS", 1L);

            tableDaily.upsert(dayTuple);

            Tuple hourTuple = tableHourly.createTuple();
            hourTuple.putInteger("HOUR", hourPickup);
            hourTuple.putLong("TRIPS", 1L);

            tableHourly.upsert(hourTuple);
        }
        catch (Throwable e) {
            System.out.println("Line: "+Arrays.toString(rowParts)+" Tuple: "+tuple.toString());
            e.printStackTrace();
        }
    }


    private void initTables(Session session){

      table = session.database().getTable(GISDemoConstants.TABLE);
      tableWeekly = session.database().getTable(GISDemoConstants.TABLE_WEEKLY);
      tableDaily = session.database().getTable(GISDemoConstants.TABLE_DAILY);
      tableHourly = session.database().getTable(GISDemoConstants.TABLE_HOURLY);

    }

  @Override
  public boolean hasHeader() {
    return HAS_HEADER;
  }

    @Override
    public int getCommitThreshold() {
        return COMMIT_THRESHOLD;
    }
}
