package com.leanxcale.service.creator.impl;

import java.io.IOException;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Database;
import com.leanxcale.kivi.session.Credentials;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.service.creator.MetadataCreator;

public abstract class AbstractMetadataCreatorImpl implements MetadataCreator {

    private String table;
    private String tableWeekly;
    private String tableDaily;
    private String tableHourly;


    public String getTable() {
        return table;
    }

    public String getTableDaily() {
        return tableDaily;
    }

    public String getTableHourly() {
        return tableHourly;
    }

    public String getTableWeekly() {
        return tableWeekly;
    }

    protected void setTable(String table) {
        this.table = table;
    }

    public void setTableWeekly(String tableWeekly) {
        this.tableWeekly = tableWeekly;
    }

    public void setTableDaily(String tableDaily) {
        this.tableDaily = tableDaily;
    }

    public void setTableHourly(String tableHourly) {
        this.tableHourly = tableHourly;
    }


    @Override
    public void cleanDemo(String lxUrl, String database, String user, String password) throws Exception {

        setTable();

        Settings settings = new Settings();
        Credentials credentials = new Credentials();
        credentials.setDatabase(database).setUser(user);
        credentials.setDatabase(database).setPass(password.toCharArray());
        settings.credentials(credentials);
        settings.transactional();

        Session session = null;
        try {
            session = SessionFactory.newSession(lxUrl, settings);
            Database databaseInstance = session.database();
            if (databaseInstance.tableExists(table)) {
                initTable(session);
            }
            createTable(session);
            session.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public abstract void setTable();

    public abstract void createTable(Session session);

    public abstract void initTable(Session session);

}
