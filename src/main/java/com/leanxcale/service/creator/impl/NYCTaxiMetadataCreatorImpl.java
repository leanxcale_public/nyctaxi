package com.leanxcale.service.creator.impl;

import com.leanxcale.constants.GISDemoConstants;
import com.leanxcale.kivi.database.*;
import com.leanxcale.kivi.deployment.Deployment;
import com.leanxcale.kivi.deployment.Region;
import com.leanxcale.kivi.deployment.Replica;
import com.leanxcale.kivi.session.Session;

import com.leanxcale.kivi.session.impl.TransactionProvider;
import com.leanxcale.kivi.tuple.TupleKey;
import java.util.Arrays;
import java.util.stream.IntStream;


import static com.leanxcale.kivi.database.constraint.Constraints.autoGeohash;
import static com.leanxcale.kivi.database.constraint.Constraints.onlineAggregate;
import static com.leanxcale.kivi.database.trigger.deltas.Aggregates.*;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class NYCTaxiMetadataCreatorImpl extends AbstractMetadataCreatorImpl {

    @Override
    public void setTable() {
        setTable(GISDemoConstants.TABLE);
        setTableWeekly(GISDemoConstants.TABLE_WEEKLY);
        setTableDaily(GISDemoConstants.TABLE_DAILY);
        setTableHourly(GISDemoConstants.TABLE_HOURLY);
    }

    @Override
    public void createTable(Session session) {
        // Gets the database object (to access the schema)
        Database database = session.database();

        // Checks the existence of the table
        if (!database.tableExists(getTable())) {

            // If the table does not exist creates it.
            database.createTable(getTable(),
                    Arrays.asList(new Field("GHPICKUP", Type.STRING), new Field("PICKUP_DATETIME", Type.TIMESTAMP),
                            new Field("MEDALLION", Type.LONG), new Field("HACK_LICENSE", Type.LONG)), // PK
                    Arrays.asList(new Field("VENDOR_ID", Type.STRING),
                            new Field("RATE_CODE", Type.INT),new Field("STORE_AND_FWD_FLAG", Type.STRING),
                            new Field("DROPOFF_DATETIME", Type.TIMESTAMP),new Field("PASSENGER_COUNT", Type.INT),
                            new Field("TRIP_TIME_IN_SECS", Type.INT), new Field("TRIP_DISTANCE", Type.DOUBLE),
                            new Field("PICKUP_LONGITUDE", Type.DOUBLE), new Field("PICKUP_LATITUDE", Type.DOUBLE),
                            new Field("DROPOFF_LONGITUDE", Type.DOUBLE), new Field("DROPOFF_LATITUDE", Type.DOUBLE),
                            new Field("GHDROPOFF", Type.STRING),
                            new Field("SCORE", Type.DOUBLE)),
                    Arrays.asList(
                            autoGeohash("GHPICKUP", "PICKUP_LONGITUDE", "PICKUP_LATITUDE", false),
                            autoGeohash("GHDROPOFF", "DROPOFF_LONGITUDE", "DROPOFF_LATITUDE", true),
                            onlineAggregate("TRIP_DATA_COUNT",emptyList(), count("TOTAL"),max("PICKUP_DATETIME","LAST")),
                            onlineAggregate("TRIP_DATA_MEDALLION",singletonList("MEDALLION"), sum("SCORE", "SUM_SCORE"), countField("SCORE","TOTAL_SCORE")),
                            onlineAggregate("TRIP_DATA_VENDOR", singletonList("VENDOR_ID"), sum("PASSENGER_COUNT", "SUM_PASSENGER"), countField("PASSENGER_COUNT","TOTAL_PASSENGER"))
                    ),
                    "PICKUP_DATETIME");

            database.createTable(getTableWeekly(),
                    singletonList(new Field("WEEK", Type.INT)),
                    singletonList(new Field("TRIPS", Type.LONG, DeltaType.ADD)));

            database.createTable(getTableDaily(),
                singletonList(new Field("DAY", Type.INT)),
                singletonList(new Field("TRIPS", Type.LONG, DeltaType.ADD)));

            database.createTable(getTableHourly(),
                singletonList(new Field("HOUR", Type.INT)),
                singletonList(new Field("TRIPS", Type.LONG, DeltaType.ADD)));

            splitTable(session, session.database().getTable(GISDemoConstants.TABLE));
        }
    }

    @Override
    public void initTable(Session session) {
        session.database().dropTable(GISDemoConstants.TABLE);
        session.database().dropTable(GISDemoConstants.TABLE_DAILY);
        session.database().dropTable(GISDemoConstants.TABLE_HOURLY);
        session.database().dropTable(GISDemoConstants.TABLE_WEEKLY);
    }

    private void splitTable(Session session, Table table){

        String[] splitPoints = new String[]{"dr5rsjbn9uwv","dr5rsw2h0kgx", "dr5ru4xt88yz","dr5rudewpffs", "dr5rumhh9305", "dr5ruvrun8dm","dr5ryyxq0vsx"};

        Deployment deployment = session.deployment();

        Region region = deployment.getRegions(table).get(0);
        String firstServer = region.getReplicas().get(0).getServer();

        String[] servers = deployment.getServers().toArray(new String[]{});

        TupleKey tupleKey = table.createTupleKey();
        tupleKey.putIgnore("PICKUP_DATETIME");
        tupleKey.putIgnore("MEDALLION");
        tupleKey.putIgnore("HACK_LICENSE");

        int indexServers = IntStream.range(0,servers.length)
                .filter(i->firstServer.equals(servers[i]))
                .findFirst()
                .getAsInt();

        for(int i=0;i<splitPoints.length;i++){

            String serverDest = servers[(++indexServers)%servers.length];

            tupleKey.putString("GHPICKUP",splitPoints[i]);
            region = region.split(tupleKey);

            Replica replica = region.getReplicas().get(0);

            if(!replica.getServer().equals(serverDest)){

                replica.move(serverDest);
            }
        }
    }
}
