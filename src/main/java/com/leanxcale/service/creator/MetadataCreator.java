package com.leanxcale.service.creator;

import java.io.IOException;

import com.leanxcale.exception.LeanxcaleException;

public interface MetadataCreator {
	
	public void cleanDemo(String lxUrl, String database, String user, String password) throws Exception;

}
