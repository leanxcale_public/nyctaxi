package com.leanxcale.service.factory.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.leanxcale.service.creator.MetadataCreator;
import com.leanxcale.service.factory.MetadataCreatorFactory;
import com.leanxcale.service.loader.DataLoader;

@Service
public class MetadataCreatorFactoryImpl implements MetadataCreatorFactory{

	private static Map<String, String> classes = null;
	private static final String propsName = "factory/metadataCreatorFactory.properties";
	private static Object lock = new Object();

	@Override
	public MetadataCreator getCreator(String name) throws Exception {
		synchronized (lock) {
			if (classes == null) loadDataLoaderFactoryProperties();
		}
		return (MetadataCreator)Class.forName(classes.get(name)).newInstance();
	}
	
	private void loadDataLoaderFactoryProperties () throws IOException {
		
		Properties prop = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsName);
		prop.load(in);
		
		classes = new HashMap<String, String>();
		Enumeration<String> enums = (Enumeration<String>) prop.propertyNames();
		while (enums.hasMoreElements()) {
			String key = enums.nextElement();
			classes.put(key, prop.getProperty(key));
		}
	}


}
