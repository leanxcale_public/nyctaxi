package com.leanxcale.service.factory.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import java.util.concurrent.ConcurrentHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.leanxcale.service.factory.DataLoaderFactory;
import com.leanxcale.service.loader.DataLoader;

@Service
public class DataLoaderFactoryImpl implements DataLoaderFactory {
	
	private static final String propsName = "factory/dataLoaderFactory.properties";
	
	private static ConcurrentHashMap<String, String> classes = null;
	private static Object lock = new Object();
	
	@Override
	public DataLoader getLoader(String name) throws Exception {
		synchronized (lock) {
			if (classes == null) loadDataLoaderFactoryProperties();
		}
		return (DataLoader)Class.forName(classes.get(name)).newInstance();
	}
	
	private void loadDataLoaderFactoryProperties () throws IOException {
		
		Properties prop = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsName);
		prop.load(in);
		
		classes = new ConcurrentHashMap<String, String>();
		Enumeration<String> enums = (Enumeration<String>) prop.propertyNames();
		while (enums.hasMoreElements()) {
			String key = enums.nextElement();
			classes.put(key, prop.getProperty(key));
		}
	}

}
