package com.leanxcale.service.factory;

import java.io.IOException;

import com.leanxcale.service.loader.DataLoader;


public interface DataLoaderFactory {
	
	public DataLoader getLoader(String name) throws Exception;

}
